﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Tools : MonoBehaviour
{

	[MenuItem("Tools/Clear PlayerPrefs")]
	static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
	}
}
