﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Responsivator : MonoBehaviour
{
    float lastWidth;

    const float REFERENCE_RESOLUTION = 1024f / 768f;

    void Update()
    {
        if (lastWidth != Screen.width)
        {
            float scaleFactor = ((float)Screen.width / (float)Screen.height) * REFERENCE_RESOLUTION;
            transform.localScale = Vector3.one * scaleFactor;
            lastWidth = Screen.width;
        }
    }
}
