﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeAddedEffect : MonoBehaviour
{
    public ParticleSystem regularTimeAdded;
    public ParticleSystem bonusTimeAdded;

    public void FireAt(Vector2 position, bool bonus)
    {
        this.transform.position = position;
        regularTimeAdded.gameObject.SetActive(false);
        bonusTimeAdded.gameObject.SetActive(false);

        StartCoroutine(FireHandler(bonus));
    }

    IEnumerator FireHandler(bool bonus)
    {
        if (!bonus)
            regularTimeAdded.gameObject.SetActive(true);
        else
            bonusTimeAdded.gameObject.SetActive(true);
        yield break;
    }
}
