﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissEffect : MonoBehaviour
{
    public ParticleSystem miss;

    public void FireAt(Vector2 position)
    {
        this.transform.position = position;
        miss.gameObject.SetActive(false);

        StartCoroutine(FireHandler());
    }

    IEnumerator FireHandler()
    {
        miss.gameObject.SetActive(true);
        yield break;
    }
}
