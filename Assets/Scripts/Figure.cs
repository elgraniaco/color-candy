﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Figure : MonoBehaviour
{
    public FigureType type;
    public Transform parts;
    public SpriteRenderer background;
    public SpriteRenderer shadow;

    public event Action<Figure, Color> OnFigureTapped;

    public Color[] Colors
    {
        get;
        private set;
    }

    public int SortingOrder
    {
        get
        {
            return sortingOrder;
        }
        set
        {
            int newSorting = SORTING_DISTANCE * value;
            foreach (Transform t in parts)
            {
                SpriteRenderer part = t.GetComponent<SpriteRenderer>();
                part.sortingOrder = (part.sortingOrder - sortingOrder * SORTING_DISTANCE) + newSorting;
            }
            background.sortingOrder = (background.sortingOrder - sortingOrder * SORTING_DISTANCE) * sortingOrder;
            shadow.sortingOrder = (shadow.sortingOrder - sortingOrder * SORTING_DISTANCE) * sortingOrder;
            sortingOrder = value;
        }
    }
    private int sortingOrder = 0;
    private const int SORTING_DISTANCE = 100;

    private Collider2D[] colliders;
    private Dictionary<Color, bool> disabledColors;
    private Dictionary<GameObject, Color> ColorsForParts;
    private bool hidden = true;
    private Vector3 originalScale;
    private Vector3 originalPosition;
    private Dictionary<int, IEnumerator> beatCoroutines = new Dictionary<int, IEnumerator>();
    private IEnumerator latestAction;

    void Update()
    {
        // if the figure is hidden don't check for input on it
        if (hidden)
            return;

        bool uiBlocksInput = false;
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (Input.touches.Length > 0 && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
#elif UNITY_EDITOR
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
#endif
            uiBlocksInput = true;

        if (!uiBlocksInput && Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null && hit.collider.transform.parent.parent == transform)
            {
                Color selectedColor = ColorsForParts[hit.collider.gameObject];
                bool disabledColor = false;
                bool colorPresent = disabledColors.TryGetValue(selectedColor, out disabledColor);
                if (!colorPresent || !disabledColor)
                    OnFigureTapped(this, selectedColor);
            }
        }
    }

    public void Init()
    {
        colliders = new Collider2D[parts.childCount];
        disabledColors = new Dictionary<Color, bool>(colliders.Length);

        Colors = new Color[parts.childCount];
        ColorsForParts = new Dictionary<GameObject, Color>(parts.childCount);
        for (int i = 0; i < parts.childCount; i++)
        {
            SetColor(i, GameColor.Red);
            colliders[i] = parts.GetChild(i).GetComponent<Collider2D>();
        }

        originalScale = transform.localScale;
        originalPosition = transform.localPosition;

        ToggleColliders(false);
        ToggleParts(false);
    }

    public void SetColor(int partIndex, Color color)
    {
        StopAllCoroutines();

        Transform part = parts.GetChild(partIndex);
        part.GetComponent<SpriteRenderer>().color = color;
        ColorsForParts[part.gameObject] = color;
        Colors[partIndex] = color;
    }

    public void Show(float transitionDuration = 0)
    {
        if (!hidden)
            return;

        hidden = false;
        ToggleColliders(true);
        if (latestAction != null)
            StopCoroutine(latestAction);

        SortingOrder = 1;
        ToggleParts(true);

        if (transitionDuration > 0)
        {
            latestAction = ShowHandler(transitionDuration);
            StartCoroutine(latestAction);
        }
    }

    private IEnumerator ShowHandler(float transitionDuration)
    {
        yield return StartCoroutine(LerpScale(originalScale * .5f, originalScale, transitionDuration));
    }

    public void Hide(float transitionDuration = 0)
    {
        if (hidden)
            return;

        hidden = true;
        ToggleColliders(false);
        if (latestAction != null)
            StopCoroutine(latestAction);

        SortingOrder = 0;
        if (transitionDuration == 0)
            ToggleParts(false);
        else
        {
            latestAction = HideHandler(transitionDuration);
            StartCoroutine(latestAction);
        }
    }

    private IEnumerator HideHandler(float transitionDuration)
    {
        float segmentDuration = transitionDuration / 3f;
        yield return StartCoroutine(LerpScale(originalScale, originalScale * 1.2f, segmentDuration));
        yield return new WaitForSeconds(segmentDuration);
        yield return StartCoroutine(LerpScale(originalScale * 1.2f, originalScale, segmentDuration));

        ToggleParts(false);
    }

    public void Shake(float offset, float duration)
    {
        StartCoroutine(ShakeHandler(offset, duration));
    }

    private IEnumerator LerpScale(Vector3 initial, Vector3 final, float seconds)
    {
        WaitForEndOfFrame EOF = new WaitForEndOfFrame();
        float delta = 0;
        while (delta < 1)
        {
            delta += Time.deltaTime / seconds;
            transform.localScale = Vector3.Lerp(initial, final, delta);
            yield return EOF;
        }
        transform.localScale = final;
    }

    private IEnumerator ShakeHandler(float offset, float duration)
    {
        ToggleColliders(false);

        duration /= 3f;

        Vector3 left = new Vector3
            (
                originalPosition.x - offset,
                originalPosition.y,
                originalPosition.z
            );

        Vector3 right = new Vector3
            (
                originalPosition.x + offset,
                originalPosition.y,
                originalPosition.z
            );

        WaitForEndOfFrame EOF = new WaitForEndOfFrame();
        float delta = 0;
        while (delta < 1)
        {
            delta += Time.deltaTime / duration;
            transform.localPosition = Vector3.Lerp(originalPosition, right, delta);
            yield return EOF;
        }

        delta = 0;
        while (delta < 1)
        {
            delta += Time.deltaTime / duration;
            transform.localPosition = Vector3.Lerp(right, left, delta);
            yield return EOF;
        }

        delta = 0;
        while (delta < 1)
        {
            delta += Time.deltaTime / duration;
            transform.localPosition = Vector3.Lerp(left, originalPosition, delta);
            yield return EOF;
        }

        transform.localPosition = this.originalPosition;

        ToggleColliders(true);
    }

    private void ToggleParts(bool on)
    {
        foreach (Transform t in parts)
            t.gameObject.SetActive(on);
        background.gameObject.SetActive(on);
        shadow.gameObject.SetActive(on);
    }

    private void ToggleColliders(bool on)
    {
        for (int i = 0; i < colliders.Length; i++)
            colliders[i].enabled = on;
    }

    public void EnableColor(Color color)
    {
        disabledColors[color] = false;
    }

    public void DisableColor(Color color)
    {
        disabledColors[color] = true;
    }

    public bool IsColorEnabled(Color color)
    {
        return disabledColors[color];
    }

    public void BeatColor(Color color, int times, float speed)
    {
        for (int i = 0; i < Colors.Length; i++)
            if (color == Colors[i])
            {
                IEnumerator hum = BeatHandler(i, times, speed);
                beatCoroutines[i] = hum;
                StartCoroutine(hum);
            }
    }

    public void StopColorBeat(Color color)
    {
        for (int i = 0; i < Colors.Length; i++)
            if (color == Colors[i])
            {
                StopCoroutine(beatCoroutines[i]);
                parts.GetChild(i).GetComponentInChildren<SpriteRenderer>(true).color = Colors[i];
                beatCoroutines.Remove(i);
            }
    }

    public void StopBeat()
    {
        for (int i = 0; i < Colors.Length; i++)
        {
            IEnumerator beat;
            beatCoroutines.TryGetValue(i, out beat);
            if (beat != null)
            {
                StopCoroutine(beat);
                beatCoroutines.Remove(i);
            }
            parts.GetChild(i).GetComponentInChildren<SpriteRenderer>(true).color = Colors[i];
        }
    }

    private IEnumerator BeatHandler(int index, int times, float speed)
    {
        WaitForEndOfFrame EOF = new WaitForEndOfFrame();
        float delta = 0;
        SpriteRenderer sr = parts.GetChild(index).GetComponentInChildren<SpriteRenderer>(true);
        Color initialColor = Colors[index];
        initialColor.a = 1;
        Color finalColor = Colors[index] * .8f;
        finalColor.a = 1;
        for (int i = 0; i < times; i++)
        {
            delta = 0;
            while (delta < 1)
            {
                delta += Time.deltaTime * speed;
                sr.color = Color.Lerp(initialColor, finalColor, delta);
                yield return EOF;
            }
            sr.color = finalColor;

            delta = 0;
            while (delta < 1)
            {
                delta += Time.deltaTime * speed;
                sr.color = Color.Lerp(finalColor, initialColor, delta);
                yield return EOF;
            }
            sr.color = initialColor;
        }

        beatCoroutines.Remove(index);
    }
}
