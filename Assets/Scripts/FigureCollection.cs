﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureCollection : MonoBehaviour
{
    private struct UnlockThreshold
    {
        public FigureType type;
        public int threshold;

        public UnlockThreshold(FigureType type, int threshold)
        {
            this.type = type;
            this.threshold = threshold;
        }
    }

    private List<UnlockThreshold> unlockThresholds = new List<UnlockThreshold>();

    private List<Figure> allFigures;
    public List<Figure> AllFigures
    {
        get
        {
            if (allFigures == null)
                allFigures = new List<Figure>(GetComponentsInChildren<Figure>(true));
            return allFigures;
        }
    }

    public void SetFigureThreshold(FigureType type, int threshold)
    {
        int i = 0;
        for (i = 0; i < unlockThresholds.Count; i++)
            if (threshold < unlockThresholds[i].threshold)
                break;
        unlockThresholds.Insert(i, new UnlockThreshold(type, threshold));
    }

    public int GetThreshold(FigureType type)
    {
        return unlockThresholds.Find(elem => { return elem.type == type; }).threshold;
    }

    public List<Figure> GetUnlockedFiguresBy(int matches)
    {
        // get max unlocked figure according to the amount of matches
        int maxUnlockedFigure = 0;
        for (maxUnlockedFigure = 0; maxUnlockedFigure < unlockThresholds.Count; maxUnlockedFigure++)
            if (matches < unlockThresholds[maxUnlockedFigure].threshold)
            {
                maxUnlockedFigure--;
                break;
            }
        maxUnlockedFigure = Mathf.Clamp(maxUnlockedFigure, 0, unlockThresholds.Count - 1);

        List<Figure> ret = new List<Figure>();
        for (int i = 0; i <= maxUnlockedFigure; i++)
            ret.Add(Get(unlockThresholds[i].type));
        return ret;
    }

    public Figure GetUnlockedFigureBy(int matches)
    {
        // get max unlocked figure according to the amount of matches
        int maxUnlockedFigure = 0;
        for (maxUnlockedFigure = 0; maxUnlockedFigure < unlockThresholds.Count; maxUnlockedFigure++)
            if (matches < unlockThresholds[maxUnlockedFigure].threshold)
            {
                maxUnlockedFigure--;
                break;
            }
        UnlockThreshold unlockThreshold = unlockThresholds[Mathf.Clamp(maxUnlockedFigure, 0, unlockThresholds.Count - 1)];
        return Get(unlockThreshold.type);
    }

    public Figure GetNextUnlockableFigureBy(int matches)
    {
        for (int i = 0; i < unlockThresholds.Count; i++)
            if (matches < unlockThresholds[i].threshold)
                return i <= unlockThresholds.Count - 1 ? Get(unlockThresholds[i].type) : null;
        return null;
    }

    public Figure Get(FigureType type)
    {
        return AllFigures.Find(elem => { return elem.type == type; });
    }

    public bool IsExactThreshold(int matches)
    {
        return unlockThresholds.FindIndex(elem => { return matches == elem.threshold; }) != -1;
    }
}
