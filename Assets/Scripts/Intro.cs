﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : StateMachineBehaviour
{
    private bool changeCalled = false;
    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime >= 1f && !changeCalled)
        {
            SceneManager.LoadSceneAsync("Main").allowSceneActivation = true;
            changeCalled = true;
        }
    }
}
