﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public AudioSource matchAudioSource;
    public AudioSource gameAudioSource;

    public AudioClip[] matchSFXs;
    public AudioClip missSFX;
    public AudioClip newRecordSFX;
    public AudioClip countSFX;
    public AudioClip noRecordSFX;
    public AudioClip timeoutSFX;
    public AudioClip unlockSFX;

    public void PlayMatchSFX(int soundIndex, float pitch)
    {
        matchAudioSource.pitch = pitch;
        matchAudioSource.PlayOneShot(matchSFXs[soundIndex]);
    }

    public void PlayUnlockSFX(float pitch)
    {
        matchAudioSource.pitch = pitch;
        matchAudioSource.PlayOneShot(unlockSFX);
    }

    public void PlayMissSFX()
    {
        gameAudioSource.PlayOneShot(missSFX);
    }

    public void PlayNewRecordSFX()
    {
        gameAudioSource.PlayOneShot(newRecordSFX);
    }

    public void PlayNoRecordSFX()
    {
        gameAudioSource.PlayOneShot(noRecordSFX);
    }

    public void PlayCountSFX()
    {
        gameAudioSource.PlayOneShot(countSFX);
    }

    public void PlayTimeoutSFX()
    {
        gameAudioSource.PlayOneShot(timeoutSFX);
    }

    public float Volume
    {
        get
        {
            return gameAudioSource.volume;
        }
        set
        {
            gameAudioSource.volume = value;
            matchAudioSource.volume = value;
        }
    }
}
