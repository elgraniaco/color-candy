﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public Vector3 originalScale;
    public FigureType type;

    private Color originalColor;
    private const float MAX_SCALE = 50f;
    private IEnumerator beatAction;

    public SpriteRenderer SpriteRenderer
    {
        get
        {
            if (spriteRenderer == null)
                spriteRenderer = GetComponent<SpriteRenderer>();
            return spriteRenderer;
        }
    }
    private SpriteRenderer spriteRenderer;

    public Color Color
    {
        get
        {
            return originalColor;
        }
        set
        {
            SpriteRenderer.color = value;
            originalColor = value;
        }
    }

    public void InstantScaleUp()
    {
        transform.localScale = Vector3.one * MAX_SCALE;
    }

    public void ScaleUp()
    {
        transform.localScale = originalScale;
        StopAllCoroutines();
        StartCoroutine(ScaleUpHandler());
    }

    private IEnumerator ScaleUpHandler()
    {
        while (true)
        {
            transform.localScale += Vector3.one * Time.deltaTime * 40f;

            if (transform.localScale.x <= MAX_SCALE)
                yield return null;
            else
                yield break;
        }
    }

    public void Hide()
    {
        transform.localScale = originalScale;
        SpriteRenderer.color = originalColor;
        StopAllCoroutines();
    }

    public void Beat(int times, float speed)
    {
        if (beatAction != null)
            return;
        beatAction = HumHandler(times, speed);
        StartCoroutine(beatAction);
    }

    public void StopBeat()
    {
        if (beatAction != null)
        {
            StopCoroutine(beatAction);
            beatAction = null;
        }
        SpriteRenderer.color = originalColor;
    }

    private IEnumerator HumHandler(int times, float speed)
    {
        WaitForEndOfFrame EOF = new WaitForEndOfFrame();
        float delta = 0;
        Color initialColor = originalColor;
        Color finalColor = originalColor * .8f;
        finalColor.a = 1;
        for (int i = 0; i < times; i++)
        {
            delta = 0;
            while (delta < 1)
            {
                delta += Time.deltaTime * speed;
                SpriteRenderer.color = Color.Lerp(initialColor, finalColor, delta);
                yield return EOF;
            }
            SpriteRenderer.color = finalColor;

            delta = 0;
            while (delta < 1)
            {
                delta += Time.deltaTime * speed;
                SpriteRenderer.color = Color.Lerp(finalColor, initialColor, delta);
                yield return EOF;
            }
            SpriteRenderer.color = initialColor;
        }
        beatAction = null;
    }
}
