﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FigureProgressBar : MonoBehaviour
{
    public Slider progressSlider;
    public Image[] figures;
    public Slider[] figureSliders;
    public AudioPlayer audioPlayer;

    private int unlocked = 0;

    // for editor testing purposes
#if UNITY_EDITOR
    public bool test = false;
    [Range(0, 5)]
    public int testWith = 0;

    private void Update()
    {
        if (test)
        {
            Unlock(testWith);
            test = false;
        }
    }
#endif

    private struct Threshold
    {
        public float threshold;
        public IEnumerator coroutine;
    }

    public void Unlock(int numberOfFigures)
    {
        unlocked = numberOfFigures;
        StartCoroutine(UnlockHandler());
    }

    public void Reset()
    {
        // reset
        progressSlider.value = 0f;
        foreach (Slider s in figureSliders)
            s.value = 0;
        foreach (Image i in figures)
            i.color = Color.grey;
        StopAllCoroutines();
    }

    IEnumerator UnlockHandler()
    {
        // run the horizontal progress bar
        StartCoroutine(FillProgress(progressSlider, unlocked * (1f / figures.Length), 1f));

        // store when the figures will play their unlock animation
        // and which are their respective coroutines
        List<Threshold> unlockCoroutines = new List<Threshold>();
        for (int i = 0; i < unlocked; i++)
            unlockCoroutines.Add(
                new Threshold()
                {
                    threshold = i * (1f / figures.Length),
                    coroutine = UnlockFigure(i)
                });

        // when each unlock threshold is reached by the horizontal bar
        // unlock the corresponding figure
        int j = 0;
        while (true)
        {
            if (j == unlockCoroutines.Count)
                break;

            if (progressSlider.value > unlockCoroutines[j].threshold)
            {
                StartCoroutine(unlockCoroutines[j].coroutine);
                j++;
            }

            yield return null;
        }
    }
	
    // generic fill method for sliders
    IEnumerator FillProgress(Slider slider, float value, float speed)
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * speed;
            slider.value = Mathf.Lerp(0, value, t);
            yield return null;
        }
        slider.value = value;
    }
    
    // unlock the corresponding figure (also plays sound)
    IEnumerator UnlockFigure(int index)
    {
        Image figure = figures[index];
        yield return StartCoroutine(FillProgress(figureSliders[index], 1f, 2f));

        bool sfxFired = false;
        float t = 0;
        float deltaColor = 0;
        float deltaTransform = 0;
        while (t < 1)
        {
            t += Time.deltaTime;

            // change color
            deltaColor += Time.deltaTime * 4f;
            figure.color = Color.Lerp(Color.grey, Color.white, deltaColor);

            // change scale and rotation
            deltaTransform += Time.deltaTime * 4f;
            if (deltaTransform < .5f)
            {
                figure.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 2f, deltaTransform);
                figure.transform.eulerAngles = Vector3.Lerp(Vector3.forward * -30f, Vector3.forward * 30f, deltaTransform);
            }
            else
            {
                figure.transform.localScale = Vector3.Lerp(Vector3.one * 2f, Vector3.one, deltaTransform);
                figure.transform.eulerAngles = Vector3.Lerp(Vector3.forward * 30f, Vector3.zero, deltaTransform);
            }

            // fire sounds according to index
            if (deltaColor >= .5f && !sfxFired)
            {
                audioPlayer.PlayMatchSFX(index, 1f);
                sfxFired = true;
            }

            yield return null;
        }
        figure.color = Color.white;
        figure.transform.localScale = Vector3.one;
    }
}
