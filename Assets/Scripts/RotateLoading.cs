﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLoading : MonoBehaviour
{ 
	// Update is called once per frame
	IEnumerator Start()
    {
        RectTransform rectTransform = (RectTransform)transform;
		while (true)
        {
            rectTransform.localEulerAngles += new Vector3(0, 0, -20f * Time.deltaTime);
            yield return null;
        }
	}
}
