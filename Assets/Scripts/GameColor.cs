﻿using UnityEngine;

public class GameColor
{
    public static Color Yellow
    {
        get
        {
            return ToColor(0xFCEF8A);
        }
    }

    public static Color Red
    {
        get
        {
            return ToColor(0xEA7070);
        }
    }

    public static Color Green
    {
        get
        {
            return ToColor(0xA5FFA9);
        }
    }

    public static Color Blue
    {
        get
        {
            return ToColor(0xA5ABEF);
        }
    }

    public static Color ToColor(int HexVal)
    {
        byte R = (byte)((HexVal >> 16) & 0xFF);
        byte G = (byte)((HexVal >> 8) & 0xFF);
        byte B = (byte)((HexVal) & 0xFF);
        return new Color32(R, G, B, 255);
    }
}
