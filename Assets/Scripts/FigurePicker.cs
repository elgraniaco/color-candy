﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigurePicker : MonoBehaviour
{
    public FigureCollection figureCollection;
    
    private Figure lastFigure = null;
    private int servedFigures = 0;
    private int matches = 0;
    private int misses = 0;

    public Figure GetFigure()
    {
        Figure ret = SelectFigure();
        SelectColors(ret);
        lastFigure = ret;
        servedFigures++;
        return ret;
    }
    
    // random from 0 to n-1 excluding 'exclude'
    private int RandomExcluding(int n, int exclude)
    {
        System.Random r = new System.Random();
        int result = r.Next(n - 1);

        for (int i = 0; i < 1; i++)
        {
            if (result < exclude)
                return result;
            result++;
        }
        return result;
    }

    private Figure SelectFigure()
    {
        // first and second figures served are triangles,
        // for tutorial purposes only the correct colors are selectable
        Figure triangle = figureCollection.Get(FigureType.Triangle);
        switch (servedFigures)
        {
            case 0:
                triangle.DisableColor(GameColor.Yellow);
                triangle.EnableColor(GameColor.Red);
                return triangle;
            case 1:
                triangle.EnableColor(GameColor.Yellow);
                triangle.DisableColor(GameColor.Red);
                return triangle;
            case 2:
                triangle.EnableColor(GameColor.Red);
                break;
        }

        // if there are a number of matches corresponding exactly the number needed to unlock
        // a new figure, show the new figure, but only if it wasn't showned in the last serving (to
        // account for misses)
        Figure lastUnlocked = figureCollection.GetUnlockedFigureBy(matches);
        if (lastFigure != lastUnlocked && figureCollection.IsExactThreshold(matches))
            return lastUnlocked;

        // select a random figure from the unlocked ones
        List<Figure> unlockedFigures = figureCollection.GetUnlockedFiguresBy(matches);
        if (unlockedFigures.Count > 1)
            unlockedFigures.Remove(lastFigure);
        return unlockedFigures[Random.Range(0, unlockedFigures.Count)];
    }

    private void SelectColors(Figure figure)
    {
        // first two figures only have 2 colors
        if (servedFigures <= 1)
        {
            for (int i = 0; i < figure.parts.childCount; i++)
                figure.SetColor(i, i < 2 ? GameColor.Yellow : GameColor.Red);
            return;
        }

        figure.SetColor(0, GameColor.Yellow);
        figure.SetColor(1, GameColor.Red);
        figure.SetColor(2, GameColor.Green);
        figure.SetColor(3, GameColor.Blue);
    }

    public void CountMatch()
    {
        matches++;
    }

    public void CountMiss()
    {
        misses++;
    }

    public void Reset()
    {
        servedFigures = 0;
        matches = 0;
        misses = 0;
    }
}
