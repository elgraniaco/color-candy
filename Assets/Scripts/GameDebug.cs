﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDebug : MonoBehaviour
{
    public Button debugBtn;
    public GameObject debugMenu;

    public Button alwaysMatchBtn;
    public Button noTimeoutBtn;
    public Button fiveXMatchesBtn;

    void Start()
    {
#if CHEATS
        gameObject.SetActive(true);
#else
        gameObject.SetActive(false);
#endif
        debugBtn.gameObject.SetActive(true);
        debugMenu.SetActive(false);
    }

    public static bool AlwaysMatch
    {
        get;
        private set;
    }

    public static bool NoTimeout
    {
        get;
        private set;
    }

    public static bool FiveXMatches
    {
        get;
        private set;
    }

    public void ToggleDebugMenu()
    {
        bool active = debugBtn.gameObject.activeSelf;
        debugBtn.gameObject.SetActive(!active);
        debugMenu.SetActive(active);
    }

    public void ToggleAlwaysMatch()
    {
        ColorBlock newColorBlock = alwaysMatchBtn.colors;
        newColorBlock.normalColor = new Color
            (
                newColorBlock.normalColor.r,
                newColorBlock.normalColor.g,
                newColorBlock.normalColor.b,
                AlwaysMatch ? .5f : 1
            );
        newColorBlock.highlightedColor = new Color
            (
                newColorBlock.highlightedColor.r, 
                newColorBlock.highlightedColor.g, 
                newColorBlock.highlightedColor.b, 
                AlwaysMatch ? .5f : 1
            );
        alwaysMatchBtn.colors = newColorBlock;

        AlwaysMatch = !AlwaysMatch;
    }

    public void ToggleNoTimeout()
    {
        ColorBlock newColorBlock = noTimeoutBtn.colors;
        newColorBlock.normalColor = new Color
            (
                newColorBlock.normalColor.r,
                newColorBlock.normalColor.g,
                newColorBlock.normalColor.b,
                NoTimeout ? .5f : 1
            );
        newColorBlock.highlightedColor = new Color
            (
                newColorBlock.highlightedColor.r,
                newColorBlock.highlightedColor.g,
                newColorBlock.highlightedColor.b,
                NoTimeout ? .5f : 1
            );
        noTimeoutBtn.colors = newColorBlock;

        NoTimeout = !NoTimeout;
    }

    public void ToggleFiveXMatches()
    {
        ColorBlock newColorBlock = fiveXMatchesBtn.colors;
        newColorBlock.normalColor = new Color
            (
                newColorBlock.normalColor.r,
                newColorBlock.normalColor.g,
                newColorBlock.normalColor.b,
                FiveXMatches ? .5f : 1
            );
        newColorBlock.highlightedColor = new Color
            (
                newColorBlock.highlightedColor.r,
                newColorBlock.highlightedColor.g,
                newColorBlock.highlightedColor.b,
                FiveXMatches ? .5f : 1
            );
        fiveXMatchesBtn.colors = newColorBlock;

        FiveXMatches = !FiveXMatches;
    }

    public void ClearRecord()
    {
        PlayerPrefs.DeleteAll();
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
