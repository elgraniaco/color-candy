﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOutEffect : MonoBehaviour
{
    public ParticleSystem timeOut;

    public void Fire()
    {
        timeOut.gameObject.SetActive(false);

        StartCoroutine(FireHandler());
    }

    IEnumerator FireHandler()
    {
        timeOut.gameObject.SetActive(true);
        yield break;
    }
}
