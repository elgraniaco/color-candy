﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialServer : MonoBehaviour
{
    public Animator pointerAnimator;

    IEnumerator lastAction;
    Figure lastFigure;
    Background lastBackground;

    public void Step(int matches, Figure figure, Background background)
    {
        if (lastAction != null)
        {
            StopCoroutine(lastAction);
            lastFigure.StopBeat();
            lastBackground.StopBeat();
            lastAction = null;
        }

        lastFigure = figure;
        lastBackground = background;

        if (matches <= 1)
        {
            lastAction = StepHandler(matches, figure, background);
            StartCoroutine(lastAction);

            if (matches == 0)
            {
                pointerAnimator.ResetTrigger("Disappear");
                pointerAnimator.ResetTrigger("Appear");
                pointerAnimator.SetTrigger("Appear");
                pointerAnimator.transform.position = figure.parts.GetChild(3).position + new Vector3(1f, 0, 0);
            }
            else
            {
                pointerAnimator.SetTrigger("Disappear");
                pointerAnimator.SetTrigger("Appear");
                pointerAnimator.transform.position = figure.parts.GetChild(1).position + new Vector3(.5f, 0, 0);
            }
        }

        if (matches == 2)
        {
            pointerAnimator.ResetTrigger("Appear");
            pointerAnimator.SetTrigger("Disappear");
        }
    }

    private IEnumerator StepHandler(int matches, Figure figure, Background background)
    {
        while (true)
        {
            background.Beat(1, 2f);

            yield return new WaitForSeconds(1.5f);

            Color color = matches == 0 ? GameColor.Red : GameColor.Yellow;
            figure.BeatColor(color, 2, 3f);

            yield return new WaitForSeconds(3f);
        }
    }
}
