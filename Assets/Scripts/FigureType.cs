﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FigureType
{
    Triangle,
    Square,
    Diamond,
    Pentagon,
    Hexagon,
    Circle
}
