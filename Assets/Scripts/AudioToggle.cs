﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour
{
    public GameLogic gameLogic;
    public Image image;

    private void Start()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, !gameLogic.AudioMuted ? 0 : .6f);
    }

    public void ToggleAudio()
    {
        gameLogic.ToggleAudio();

        StopAllCoroutines();
        StartCoroutine(LerpAlpha());
    }

    private IEnumerator LerpAlpha()
    {
        WaitForEndOfFrame EOF = new WaitForEndOfFrame();
        float initialAlpha = image.color.a;
        float targetAlpha = !gameLogic.AudioMuted ? 0 : .6f;
        float delta = 0;
        while (delta < 1)
        {
            delta += Time.deltaTime * 5f;
            image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(initialAlpha, targetAlpha, delta));
            yield return EOF;
        }

        image.color = new Color(image.color.r, image.color.g, image.color.b, targetAlpha);
    }
}
