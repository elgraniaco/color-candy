﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPicker
{
    private List<Color> previousColors = new List<Color>();
    private Color lastColor;

    public Color GetRandom(params Color[] colors)
    {
        if (previousColors.Count > 0)
            lastColor = previousColors[previousColors.Count - 1];

        List<Color> trimmedList = new List<Color>(colors);
        trimmedList.RemoveAll(elem => previousColors.Contains(elem));
        if (trimmedList.Count == 0)
        {
            trimmedList = new List<Color>(colors);
            trimmedList.Remove(previousColors[previousColors.Count - 1]);
            previousColors.Clear();
        }

        Color ret = trimmedList.Count != 0 ? trimmedList[Random.Range(0, trimmedList.Count)] : lastColor;
        previousColors.Add(ret);
        return ret;
    }
}
