﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    public Text score;
    public Text record;
    public FigureCollection figureCollection;
    public EndGamePopUp endGamePopUp;
    public BackgroundChanger backgroundChanger;
    public FigurePicker figurePicker;
    public Timer timer;
    public GameUI gameUI;
    public AudioPlayer audioPlayer;
    public TutorialServer tutorialServer;

    ColorPicker colorPicker = new ColorPicker();
    Figure figure;
    Background background;
    int matches;
    bool gameStarted = false;
    Color winColor;
    int countBonus = 0;
    float bonusTimeForNextRun = 0;
    // match sfx control
    int countMatchSFX = 0;
    int countSFXcycles = 0;

    // audio player pref key
    const string MUTE_PLAYEPREF_KEY = "Mute";

    // game config
    // score required on a run to unlock figures, respectively
    readonly int[] figureUnlockCount = new int[] { 0, 6, 25, 60, 100, 150 };

    // first color
    Color FIRST_COLOR = GameColor.Red;

    // maximum and minimum time the timer can have
    const float MAX_TIME = 5f;
    const float MIN_TIME = .8f;

    // given time when matching a color
    const float MATCH_TIME_BONUS = .5f;
    // how many consecutive matches are needed to get the bonus time up
    const int BONUS_COUNTER = 4;
    // combo bonus time given when doing consecutive matches
    const float MATCH_COMBO_TIME_BONUS = 1f;
    // time penalized when missing a match
    const float MISS_TIME_PENALTY = .2f;
    // bonus free time when a figure is unlocked
    const float FIGURE_UNLOCKED_TIME_BONUS = 2f;

    // SFX initial pitch
    public const float INITIAL_PITCH = .8f;
    // SFX pitch increments
    public const float PITCH_INCREMENTS = .03f;

    public bool AudioMuted
    {
        get
        {
            if (!PlayerPrefs.HasKey(MUTE_PLAYEPREF_KEY))
                PlayerPrefs.SetInt(MUTE_PLAYEPREF_KEY, 0);

            return PlayerPrefs.GetInt(MUTE_PLAYEPREF_KEY) == 1;
        }
        private set
        {
            PlayerPrefs.SetInt(MUTE_PLAYEPREF_KEY, value ? 1 : 0);
        }
    }

    public void ToggleAudio()
    {
        audioPlayer.Volume = AudioMuted ? 1 : 0;
        AudioMuted = !AudioMuted;
    }

    // Use this for initialization
    IEnumerator Start()
    {
        // setting figure unlock thresholds
        figureCollection.SetFigureThreshold(FigureType.Triangle, figureUnlockCount[0]);
        figureCollection.SetFigureThreshold(FigureType.Square, figureUnlockCount[1]);
        figureCollection.SetFigureThreshold(FigureType.Diamond, figureUnlockCount[2]);
        figureCollection.SetFigureThreshold(FigureType.Pentagon, figureUnlockCount[3]);
        figureCollection.SetFigureThreshold(FigureType.Hexagon, figureUnlockCount[4]);
        figureCollection.SetFigureThreshold(FigureType.Circle, figureUnlockCount[5]);

        // initialize audio player
        audioPlayer.Volume = AudioMuted ? 0 : 1;


        foreach (Figure f in figureCollection.AllFigures)
        {
            f.Init();
            f.OnFigureTapped += OnFigureTapped;
            backgroundChanger.AddBackground(f);
        }

        RetrieveRecord();

        figure = figurePicker.GetFigure();
        ResetFigure();

        winColor = colorPicker.GetRandom(FIRST_COLOR);
        background = backgroundChanger.ChangeColor(winColor, figure);
        score.text = "0";

        gameUI.UnlockFigures(PlayerPrefs.GetInt("Record"));

        yield return new WaitForSeconds(.5f);

        // first tutorial call
        tutorialServer.Step(matches, figure, background);
    }

    private void AdsRewardGrantedHandler(string type, double amount)
    {
        bonusTimeForNextRun = (float)amount;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID
        // added game minimize support for android when back button is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Get the unity player activity
            AndroidJavaObject activity =
               new AndroidJavaClass("com.unity3d.player.UnityPlayer")
               .GetStatic<AndroidJavaObject>("currentActivity");

            // call activity's boolean moveTaskToBack(boolean nonRoot) function
            // documentation:
            // http://developer.android.com/reference/android/app/Activity.html#moveTaskToBack(boolean)
            activity.Call<bool>("moveTaskToBack", true);
        }
#endif

        if (gameStarted)
        {
            gameUI.GameplayMode();
            
#if CHEATS
            if (GameDebug.NoTimeout)
                timer.Pause();
            else
                timer.Continue();
#endif
            if (timer.TimeOut)
                Timeout();
        }
    }

    private void OnFigureTapped(Figure figure, Color color)
    {
        StartCoroutine(OnFigureTappedHandler(color));
    }

    private IEnumerator OnFigureTappedHandler(Color selectedColor)
    {
        if (!gameStarted)
            timer.Go(MIN_TIME, MAX_TIME, 0);
        gameStarted = true;

        bool matched = winColor == selectedColor;

#if CHEATS
        if (GameDebug.AlwaysMatch)
            matched = true;
#endif

        // check if the color selected matched
        yield return StartCoroutine(matched ? Match() : Miss());

        gameUI.UpdateRunProgress(matches);

        yield break;
    }

    private IEnumerator Match()
    {
#if CHEATS
        if (GameDebug.FiveXMatches)
            matches += 4;
#endif
        matches++;
        score.text = matches.ToString();

        figurePicker.CountMatch();

        countBonus++;
        if (countBonus >= BONUS_COUNTER)
        {
            countBonus = 0;
            timer.AddCurrentTime(MATCH_COMBO_TIME_BONUS);
        }
        else
            timer.AddCurrentTime(MATCH_TIME_BONUS);

        // play match sfx
        float currentPitch = INITIAL_PITCH + countSFXcycles * PITCH_INCREMENTS;
        audioPlayer.PlayMatchSFX(countMatchSFX, currentPitch);
        
        // update match sfx
        countMatchSFX++;
        if (countMatchSFX > 6)
        {
            countMatchSFX = 0;
            countSFXcycles++;
        }

        if (figureCollection.IsExactThreshold(matches))
        {
            timer.AddBonusTime(FIGURE_UNLOCKED_TIME_BONUS + bonusTimeForNextRun);
            audioPlayer.PlayUnlockSFX(currentPitch);

            // reset match sfx
            countMatchSFX = 0;
            countSFXcycles = 0;
        }

        yield return StartCoroutine(ChangeFigure());
    }

    private IEnumerator Miss()
    {
        figurePicker.CountMiss();

        countBonus = 0;

        // reset match sfx
        countMatchSFX = 0;
        countSFXcycles = 0;

        Handheld.Vibrate();

        audioPlayer.PlayMissSFX();

        figure.Shake(MISS_TIME_PENALTY, .2f);

        yield return new WaitForSeconds(MISS_TIME_PENALTY);

        yield return StartCoroutine(ChangeFigure());
    }

    private IEnumerator ChangeFigure()
    {
        // get new figure reference
        Figure lastFigure = figure;
        figure = figurePicker.GetFigure();

        // selecting new color and firing background change effect
        winColor = colorPicker.GetRandom(figure.Colors);
        background = backgroundChanger.ChangeColor(winColor, lastFigure);

        // call tutorial after figure change
        tutorialServer.Step(matches, figure, background);

        // figure change animation
        foreach (Figure f in figureCollection.AllFigures)
            f.Hide(.25f);

        yield return new WaitForSeconds(0.2f);

        figure.Show(.1f);
    }

    private void Timeout()
    {
        audioPlayer.PlayTimeoutSFX();
        endGamePopUp.Display(matches, matches > PlayerPrefs.GetInt("Record"));

        PersistNewRecord();
        RetrieveRecord();

        countMatchSFX = 0;
        countSFXcycles = 0;
        countBonus = 0;
        gameStarted = false;
        timer.Stop();
        bonusTimeForNextRun = 0;
    }

    private void RetrieveRecord()
    {
        if (PlayerPrefs.HasKey("Record"))
            record.text = PlayerPrefs.GetInt("Record").ToString();
        else
            record.text = "0";
    }

    private void PersistNewRecord()
    {
        if (!PlayerPrefs.HasKey("Record") || matches > PlayerPrefs.GetInt("Record"))
            PlayerPrefs.SetInt("Record", matches);
    }

    public void ResetGame()
    {
        matches = 0;

        gameUI.MenuMode();

        gameUI.UnlockFigures(PlayerPrefs.GetInt("Record"));
        score.text = matches.ToString();

        // resetting figure and figure picker
        figurePicker.Reset();
        figure = figurePicker.GetFigure();
        ResetFigure();

        // resetting background color and color changer
        backgroundChanger.Reset();
        winColor = colorPicker.GetRandom(FIRST_COLOR); ;
        background = backgroundChanger.ChangeColor(winColor, figure);

        // resetting tutorial
        tutorialServer.Step(matches, figure, background);
    }

    private void ResetFigure()
    {
        foreach (Figure f in figureCollection.AllFigures)
            f.Hide();

        figure.Show();
    }
}
