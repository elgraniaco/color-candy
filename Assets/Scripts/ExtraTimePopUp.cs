﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraTimePopUp : MonoBehaviour
{
    public Animator animator;

    public void Open()
    {
        animator.SetBool("Show", true);
    }

    public void Close()
    {
        animator.SetBool("Show", false);
    }
}
