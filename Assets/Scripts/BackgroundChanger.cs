﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundChanger : MonoBehaviour
{
    private List<Background> backgrounds = new List<Background>();
    private Dictionary<FigureType, Stack<Background>> pooledBackgrounds = new Dictionary<FigureType, Stack<Background>>();
    private Queue<Background> activeBackgrounds = new Queue<Background>();
    private int countSort = 0;

    public void AddBackground(Figure figure)
    {
        GameObject newBackgroundGO = Instantiate(figure.background.gameObject);
        newBackgroundGO.transform.SetParent(transform, false);
        newBackgroundGO.gameObject.SetActive(false);
        newBackgroundGO.transform.localEulerAngles = figure.transform.localEulerAngles;

        SpriteRenderer newBackgroundSprite = newBackgroundGO.GetComponent<SpriteRenderer>();
        newBackgroundSprite.sortingLayerName = "Background";
        newBackgroundSprite.sortingOrder = 0;

        Background newBackground = newBackgroundSprite.gameObject.AddComponent<Background>();
        newBackground.type = figure.type;
        newBackground.originalScale = figure.background.transform.localScale * .8f;

        backgrounds.Add(newBackground);
        PoolPush(newBackground);
    }

    public Background ChangeColor(Color color, Figure figure)
    {
        Background newBackground = PoolPop(backgrounds.Find(elem => elem.type == figure.type));

        if (activeBackgrounds.Count == 0)
            newBackground.InstantScaleUp();
        else
            newBackground.ScaleUp();

        if (activeBackgrounds.Count > 5)
            PoolPush(activeBackgrounds.Dequeue());

        activeBackgrounds.Enqueue(newBackground);
        newBackground.Color = color;
        newBackground.SpriteRenderer.sortingOrder = countSort++;
        return newBackground;
    }

    // background pool pop
    private Background PoolPop(Background bg)
    {
        Background newBg = null;
        if (!pooledBackgrounds.ContainsKey(bg.type))
            pooledBackgrounds[bg.type] = new Stack<Background>();

        if (pooledBackgrounds[bg.type].Count == 0)
        {
            newBg = Instantiate(bg.gameObject).GetComponent<Background>();
            newBg.type = bg.type;
            pooledBackgrounds[bg.type].Push(newBg);
            newBg.transform.SetParent(transform, false);
            newBg.gameObject.SetActive(false);
        }
        newBg = pooledBackgrounds[bg.type].Pop();
        newBg.gameObject.SetActive(true);
        return newBg;

    }

    // background pool push
    private void PoolPush(Background bg)
    {
        bg.gameObject.SetActive(false);
        if (!pooledBackgrounds.ContainsKey(bg.type))
            pooledBackgrounds[bg.type] = new Stack<Background>();
        pooledBackgrounds[bg.type].Push(bg);
    }

    public void Reset()
    {
        foreach (Background b in activeBackgrounds)
        {
            b.SpriteRenderer.sortingOrder = 0;
            b.Hide();
            PoolPush(b);
        }
        activeBackgrounds.Clear();
    }
}
