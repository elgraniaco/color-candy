﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public FigureCollection figureCollection;
    public Transform figureIcons;
    public Transform progressIcons;
    public Text progressCount;
    public Slider progressBar;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void UnlockFigures(int matches)
    {
        int unlocked = figureCollection.GetUnlockedFiguresBy(matches).Count;

        if (unlocked == 1)
            return;

        for (int i = 0; i < unlocked; i++)
            figureIcons.GetChild(Mathf.Clamp(i - 1, 0, figureIcons.childCount - 1)).GetComponent<Image>().color = Color.white;
    }

    public void UpdateRunProgress(int matches)
    {
        foreach (Transform t in progressIcons)
            t.gameObject.SetActive(false);

        Figure currentFigure = figureCollection.GetUnlockedFigureBy(matches);
        Figure nextFigure = figureCollection.GetNextUnlockableFigureBy(matches);

        if (nextFigure != null)
        {
            int currentThreshold = figureCollection.GetThreshold(currentFigure.type);
            int nextThreshold = figureCollection.GetThreshold(nextFigure.type);
            progressCount.text = (nextThreshold - matches).ToString();
            progressBar.value = (float)(matches - currentThreshold) / (float)(nextThreshold - currentThreshold);
            progressIcons.GetChild(figureCollection.GetUnlockedFiguresBy(matches).Count - 1).gameObject.SetActive(true);
        }
        else
        {
            progressCount.text = string.Empty;
            progressBar.value = 1;
            progressIcons.GetChild(progressIcons.childCount - 1).gameObject.SetActive(true);
        }
    }

    public void GameplayMode()
    {
        animator.SetBool("Game", true);
    }

    public void MenuMode()
    {
        animator.SetBool("Game", false);
    }
}
