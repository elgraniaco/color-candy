﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Image timer;
    public Image timerFill;

    private float maxTime = 20f;
    private float minTime = 20f;
    private float bonusTime = 0f;
    private float currentTime = 20f;
    private bool started = false;
    private bool paused = false;
    
    private Color NORMAL_FILL_COLOR = GameColor.ToColor(0xEA706F);
    private Color BONUS_FILL_COLOR = Color.yellow;

    private Vector3 originalLocalPos;

    public bool TimeOut
    {
        get;
        private set;
    }

    private void Start()
    {
        originalLocalPos = timer.transform.localPosition;
    }

    // Update is called once per frame
    void Update ()
    {
        // config ui timer fill
        timerFill.fillAmount = currentTime / maxTime;
        timerFill.color = bonusTime <= 0 ? NORMAL_FILL_COLOR : BONUS_FILL_COLOR;

        // if time is up or the timer hasn't been initialized don't do anything
        if (!started || TimeOut || paused)
            return;

        // if the timer has a bonus time then wait until the bonus is done
        if (bonusTime > 0)
        {
            bonusTime -= Time.deltaTime;
            currentTime = maxTime;
            return;
        }

        // subtract time until running out
        currentTime -= Time.deltaTime;

        if (currentTime <= 0)
        {
            TimeOut = true;
            currentTime = 0;
        }
	}

    public void SubtractMaxTime(float seconds)
    {
        maxTime -= seconds;
        if (maxTime <= minTime)
            maxTime = minTime;
        if (currentTime >= maxTime)
            currentTime = maxTime;
    }

    public void AddCurrentTime(float seconds)
    {
        currentTime += seconds;
        if (currentTime > maxTime)
            currentTime = maxTime;
    }

    public void SubtractCurrentTime(float seconds)
    {
        currentTime -= seconds;
        if (currentTime < 0)
            currentTime = 0;
    }

    public void AddBonusTime(float seconds)
    {
        bonusTime += seconds;
    }

    public void Go(float minTime, float maxTime, float bonusTime)
    {
        StopRing();
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.bonusTime = bonusTime;
        currentTime = maxTime;
        TimeOut = false;
        started = true;
    }

    public void Stop()
    {
        started = false;
        currentTime = 0;
        Ring();
    }

    public void Pause()
    {
        paused = true;
    }

    public void Continue()
    {
        paused = false;
    }

    void Ring()
    {
        StartCoroutine(RingHandler());
    }

    void StopRing()
    {
        StopAllCoroutines();
        timer.transform.localPosition = originalLocalPos;
        timer.transform.eulerAngles = Vector3.zero;
    }

    IEnumerator RingHandler()
    {
        float t = 0;
        int countForPause = 10;
        while (true)
        {
            t += Time.deltaTime * 7f;

            // rotating timer
            if (t <= .5f)
                timer.transform.eulerAngles = Vector3.Lerp(Vector3.forward * -15f, Vector3.forward * 15f, t / .5f);
            else
                timer.transform.eulerAngles = Vector3.Lerp(Vector3.forward * 15f, Vector3.forward * -15f, (t - .5f) / .5f);
            // reset rotation when reaching the edge
            if (t >= 1)
                t = 0;

            // shaking timer
            timer.transform.localPosition = new Vector3
            (
                originalLocalPos.x + Random.Range(-1f, 1f) * 5f,
                originalLocalPos.y + Random.Range(-1f, 1f) * 5f,
                originalLocalPos.z
            );

            // pause after 10 rotation+shakes
            countForPause--;
            if (countForPause == 0)
            {
                countForPause = 10;
                timer.transform.eulerAngles = Vector3.zero;
                timer.transform.localPosition = originalLocalPos;
                yield return new WaitForSeconds(.5f);
            }
            yield return null;
        }
    }
}
