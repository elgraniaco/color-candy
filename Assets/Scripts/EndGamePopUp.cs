﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePopUp : MonoBehaviour
{
    public Animator animator;
    public Text scoreRecord;
    public Text scoreNoRecord;
    public AudioPlayer audioPlayer;
    public FigureCollection figureCollection;
    public FigureProgressBar figureProgressBar;
    public ExtraTimePopUp extraTimePopUp;

    private bool newRecord = false;
    private int score = 0;
    private IEnumerator rollNumbersHandler;

    public void Display(int score, bool newRecord)
    {
        this.animator.SetBool("Show", true);
        this.animator.SetBool("NewRecord", newRecord);
        this.animator.SetBool("NewRecordStar", false);
        this.animator.SetBool("NoRecordStar", false);

        // reset progress bar
        this.figureProgressBar.Reset();

        if (newRecord)
            this.scoreRecord.text = "000";
        else
            this.scoreNoRecord.text = "000";

        this.newRecord = newRecord;
        this.score = score;
    }

    public void ShowNewScore()
    {
        rollNumbersHandler = ShowNewScoreHandler(newRecord, score);
        StartCoroutine(rollNumbersHandler);
    }

    private IEnumerator ShowNewScoreHandler(bool newRecord, int score)
    {
        // trigger progress bar
        this.figureProgressBar.Unlock(Mathf.Clamp(figureCollection.GetUnlockedFiguresBy(score).Count - 1, 0, figureCollection.GetUnlockedFiguresBy(score).Count - 1));

        Text scoreLabel = newRecord ? this.scoreRecord : this.scoreNoRecord;

        float scoreCount = 0;
        int lastScore = -1;
        float duration = 1f;
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            scoreCount = (int)Mathf.Lerp(0, score, progress);

            if (scoreCount > lastScore)
            {
                lastScore = (int)scoreCount;
                scoreLabel.text = lastScore.ToString("D3");
                audioPlayer.PlayCountSFX();
            }

            yield return null;
        }
        scoreLabel.text = score.ToString("D3");
        
        if (newRecord)
        {
            audioPlayer.PlayNewRecordSFX();
            this.animator.SetBool("NewRecordStar", true);
        }
        else
        {
            audioPlayer.PlayNoRecordSFX();
            this.animator.SetBool("NoRecordStar", true);
        }
    }

    public void Hide()
    {
        if (rollNumbersHandler != null)
            StopCoroutine(rollNumbersHandler);

        this.animator.SetBool("Show", false);

        this.animator.SetTrigger("ResetButtons");

        // reset progress bar
        this.figureProgressBar.Reset();
    }
}
